﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using Microsoft.VisualBasic.FileIO;


namespace SampleDataOUTSurance
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }




        public DataTable GetDataTableFromCSVFile(string csv_file_path)
        {
            DataTable csvData = new DataTable();

            try
            {
                using (TextFieldParser csvReader = new TextFieldParser(csv_file_path))
                {
                    csvReader.SetDelimiters(new string[] { "," });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    string[] colFields = csvReader.ReadFields();
                    foreach (string column in colFields)
                    {
                        DataColumn datecolumn = new DataColumn(column);
                        datecolumn.AllowDBNull = true;
                        csvData.Columns.Add(datecolumn);
                    }



                    while (!csvReader.EndOfData)
                    {
                        string[] fieldData = csvReader.ReadFields();
                        for (int i = 0; i < fieldData.Length; i++)
                        {
                            if (fieldData[i] == "")
                            {
                                fieldData[i] = null;
                            }
                        }
                        csvData.Rows.Add(fieldData);
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);  
            }
            return csvData;
        }  


        protected void BtnOut1_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable csvData = new DataTable();
                DataTable Data = new DataTable();
                Data.Columns.Add("Names", typeof(string));
                if (FileUpload1.HasFile)
                {
                    string spath = Server.MapPath("~/Files");
                    string csv_file_path = spath + "\\" + FileUpload1.FileName;
                    FileUpload1.SaveAs(csv_file_path);
                    csvData = GetDataTableFromCSVFile(csv_file_path);
                }

                for (int i = 0; i < csvData.Rows.Count; i++)
                {
                    Data.Rows.Add(csvData.Rows[i]["FirstName"]);
                    Data.Rows.Add(csvData.Rows[i]["LastName"]);
                }
                var result = Data.AsEnumerable()
                   .GroupBy(r => r.Field<string>("Names"))
                   .OrderByDescending(r => r.Count())
                   .Select(r => new
                   {
                       Str = r.Key,
                       Count = r.Count()
                   });


               // string path = @"N:\Names.txt";
                string path = @"C:\Names.txt";
                if (!File.Exists(path))
                {
                    File.Create(path).Dispose();
                    using (TextWriter tw = new StreamWriter(path))
                    {

                        foreach (var item in result)
                        {

                            tw.WriteLine(item.Str + " , " + item.Count);

                        }
                        tw.Close();
                    }

                }

                else if (File.Exists(path))
                {
                    using (TextWriter tw = new StreamWriter(path))
                    {
                        foreach (var item in result)
                        {

                            tw.WriteLine(item.Str + " , " + item.Count);

                        }
                        tw.Close();
                    }
                }
                Label1.Text = "File successful created.";
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        protected void BtnOut2_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable csvDataadd = new DataTable();
                DataTable DataAdd = new DataTable();
                DataAdd.Columns.Add("Address1", typeof(string));
                DataAdd.Columns.Add("StreetName", typeof(string));
                if (FileUpload1.HasFile)
                {
                    string spath = Server.MapPath("~/Files");
                    string csv_file_path = spath + "\\" + FileUpload1.FileName;
                    FileUpload1.SaveAs(csv_file_path);
                    csvDataadd = GetDataTableFromCSVFile(csv_file_path);
                }

                csvDataadd.DefaultView.Sort = "StreetName";
                csvDataadd = csvDataadd.DefaultView.ToTable();

              //  string path = @"N:\Address.txt";
                string path = @"C:\Address.txt";
                if (!File.Exists(path))
                {
                    File.Create(path).Dispose();
                    using (TextWriter tw = new StreamWriter(path))
                    {
                        for (int i = 0; i < csvDataadd.Rows.Count; i++)
                        {
                            tw.WriteLine(csvDataadd.Rows[i]["Address1"] + " " +csvDataadd.Rows[i]["StreetName"]);
                        }

                        tw.Close();
                    }

                }

                else if (File.Exists(path))
                {
                    using (TextWriter tw = new StreamWriter(path))
                    {
                        for (int i = 0; i < csvDataadd.Rows.Count; i++)
                        {
                            tw.WriteLine(csvDataadd.Rows[i]["Address1"] + " " + csvDataadd.Rows[i]["StreetName"]);
                        }
                        tw.Close();
                    }
                }
                Label1.Text = "File successful created.";
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
    }
}
